var express = require('express');
var path = require('path');
var app = express();
var http = require("http").Server(app);
var io = require("socket.io")(http);
var querystring = require('querystring');
var mysql = require('mysql');
var moment = require('moment');

//配置模块
var settings = require('./settings');

var dataList = [];

/*在线人员*/
var onLineUsers = [];
/* 在线人数*/
var onLineCounts = 0;

//首页路由
app.get("/" , function(req , res){
  res.sendFile(__dirname + "/chat.html");
});
//聊天记录列表路由
app.get("/chats" , function(req , res){
    var params, arr;
    req.on('data',function(data){
        //使用querystring模块中的parse方法将字符串转化为对象
        params = querystring.parse(decodeURIComponent(data));
    })
    req.on('end',function(){
        // console.log('客户端请求数据已全部接收完毕');
        get_chats(res);
    })
    //使用Access-Control-Allow-Origin解决跨域问题
    res.setHeader('Access-Control-Allow-Origin','*');
    //返回JSON数据
    res.writeHead(200,{'Content-Type' : 'application/json'});
    // res.end(JSON.stringify(dataList));
});
//资源路径
app.use(express.static(path.join(__dirname, "./app/public")));


/*io监听到存在链接，此时回调一个socket进行socket监听*/
io.on('connection', function (socket) {
    console.log('a user connected');
    var userId = socket.handshake.query.userId;
    var userName = socket.handshake.query.userName;
    var user = {"userId":userId, "userName":userName};
    // console.log('userId='+userId);
    /*不存在则加入 */
    if (!onLineUsers.hasOwnProperty(userId)) {
        onLineUsers[userId]= userName;
        onLineCounts++;
        // console.log(userId, "加入了聊天室" + onLineCounts);//在服务器控制台中打印么么么用户加入到了聊天室
        /*一个用户新加入，向所有客户端监听login的socket的实例发送响应，响应内容为一个对象*/
        io.emit('login', {onLineCounts: onLineCounts});
    }

    /*监听用户退出聊天室*/
    socket.on('disconnect', function () {
        "use strict";
        // console.log(onLineUsers);
        if (onLineUsers.hasOwnProperty(userId)) {
            delete onLineUsers[userId];
            onLineCounts--;
            io.emit('logout', {onLineUsers: onLineUsers, onLineCounts: onLineCounts, user: user});
        }
    })
    /*监听到用户发送了消息，就使用io广播信息，信息被所有客户端接收并显示。注意，如果客户端自己发送的也会接收到这个消息，故在客户端应当存在这的判断，是否收到的消息是自己发送的，故在emit时，应该将用户的id和信息封装成一个对象进行广播*/
    socket.on('message', function (obj, callback) {
        "use strict";
        obj.stime = moment().format('HH:mm:ss');
        /*监听到有用户发消息，将该消息广播给所有客户端*/
        io.emit('message', obj);
        // io.broadcast.emit('message', obj); //除开自己的广播消息
        callback();

        var ip = socket.request.connection.remoteAddress;
        if(ip.indexOf(':')>-1){
            var ips = ip.split(':');
            ip = ips[ips.length-1];
        }
        obj.ip = ip;
        obj.created_at = moment().format('YYYY-MM-DD HH:mm:ss');
        add_chat(obj); //插入数据库
    });
    // socket.on('chats', function (obj, callback) {
    //     "use strict";
    //     obj.stime = moment().format('HH:mm:ss');
    //     get_chats(callback); //回调
    // });
});
/*监听3000*/
http.listen(3000, function () {
    "use strict";
    console.log('listening 3000');
});


// // $ sudo npm install forever -g   #安装
// // $ forever start app.js          #启动
// // $ forever stop app.js           #关闭
// // $ forever start -l forever.log -o out.log -e err.log app.js   #输出日志和错误
//
//获取聊天记录列表
function get_chats(res){
    var connection = mysql.createConnection(settings.db);
    connection.connect(function(err){
        if(err){
            console.log('与mysql数据库建立连接失败');
        }else{
            // console.log('与mysql数据库建立连接成功');
            var now = new Date();
            var h = now.getHours();
            var dt = '';
            if(h<7){
                dt =moment().subtract(1, 'days').format('YYYY-MM-DD')+' 06:00:00';
            }else{
                dt =moment().format('YYYY-MM-DD')+' 06:00:00';
            }
            var sql = 'select uid userId,name userName,content msg,ip,substr(created_at,11) stime,created_at from chats';
            if(dt.length>0){
                sql += ' where created_at>\''+dt+'\'';
            }
            // sql += "";
            connection.query(sql, [], function(err,result){
                if (err) throw err;
                // for (var i = 0; i < result.length; i++) {
                //     dataList[i] = result[i].name;
                // }
                // console.log(result);
                connection.end();
                res.end(JSON.stringify(result));
            });
        }
    });
}
// function get_chats(callback){
//     var connection = mysql.createConnection(settings.db);
//     connection.connect(function(err){
//         if(err){
//             console.log('与mysql数据库建立连接失败');
//         }else{
//             // console.log('与mysql数据库建立连接成功');
//             connection.query('select uid userId,name userName,content msg,ip,created_at from chats',[], function(err,result){
//                 if (err) throw err;
//                 // for (var i = 0; i < result.length; i++) {
//                 //     dataList[i] = result[i].name;
//                 // }
//                 // console.log(result);
//                 connection.end();
//                 if(callback){
//                     callback(result); //回调
//                 }
//             });
//         }
//     });
// }
//新增聊天记录
function add_chat(data){
    dataList.push(data);
    // if(dataList.length < 5) {
    //     return;
    // }

    var values = [];
    for(var i in dataList){
        var d = dataList[i];
        values.push([d.userId, d.userName, d.msg, d.ip,d.created_at]);
    }
    // console.log(values);
    dataList = [];
    var connection = mysql.createConnection(settings.db);
    connection.connect(function(err){
        if(err){
            console.log('与mysql数据库建立连接失败');
        }else{
            // console.log('与mysql数据库建立连接成功');
            var sql = "INSERT INTO chats(uid,name,content,ip,created_at) VALUES ?";
            connection.query(sql,[values], function(err,result){
                if (err) throw err;
                connection.end();
            });
        }
    });
}
