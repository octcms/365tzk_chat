# Socket.io + NodeJS + VueJS + Mysql 在线聊天室

###### 运行命令
```bash
node app.js
```

本地浏览器打开 http://localhost:3000 即可访问

###### Nginx反向代理配置域名
请参考目录etc/nginx的conf文件内容


###### 实际应用网站
http://www.trq7.com/
